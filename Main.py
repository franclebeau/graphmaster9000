# -*- coding:Utf8 -*-
"""
Le GraphMaster9000 est un logiciel d'étude des graphes, de leurs propriétés ainsi que de leur relation au jeu de poursuite de type policier-voleur.

En utilisant les fonctions de la librairie networkX, l'utilisateur peut créer le graphe de son choix. Ce graphe peut ensuite être sauvegardé à l'aide
de la fonction src.writeGraph et comparé avec les autres graphes sauvegardés. Note : si l'utilisateur créé de nouveaux graphes, il devra ajouter lui-même à la fin
du fichier texte le temps de capture à partir du dictRelation de src.plusPetitEgalMou. La raison en est que le temps de capture n'est pas utilisé dans le logiciel,
il a été ajouté seulement aux fichiers textes existants à des fins d'observation.

Afin de déterminer si un graphe G est k-copwin ou non, utiliser la fonction src.isCopwin(G, k). Afin de trouver directement le copnumber d'un graphe G,
utiliser la fonction src.getCopNumber(G). Attention : les graphes de 40 sommets et plus vont prendre un très grand temps à analyser pour k = 3. La fonction étant
coûteuse en mémoire, tout graphe de plus de 50 sommets fera planter la plupart des ordinateurs pour k >= 3.


"""
import wx
import gui
# import src                # À utiliser au besoin
# import networkx as nx     # À utiliser au besoin

if __name__ == '__main__':
    
    app = wx.App(False)     
    frame = gui.MainWindow(None, "GraphMaster 9000")
    app.MainLoop()