# -*- coding:Utf8 -*-
__author__ = 'simfr21'
import networkx as nx
from matplotlib import pylab
from matplotlib.pyplot import axis, ylim, figure, cm, savefig, xlim
from matplotlib import pyplot as plt
import matplotlib as mpl

def main():
    createGraph()

def ranGeom():
    G=nx.random_geometric_graph(2000,0.025)
    P=G
    for i in range(0,9):
        P=nx.strong_product(G,P)

    # position is stored as node attribute data for random_geometric_graph
    pos=nx.get_node_attributes(G,'pos')

    # find node near center (0.5,0.5)
    dmin=1
    ncenter=0
    for n in pos:
        x,y=pos[n]
        d=(x-0.5)**2+(y-0.5)**2
        if d<dmin:
            ncenter=n
            dmin=d

    # color by path length from node near center
    p=nx.single_source_shortest_path_length(G,ncenter)

    figure(figsize=(8,8))
    nx.draw_networkx_edges(G,pos,nodelist=[ncenter],alpha=0.4)
    nx.draw_networkx_nodes(G,pos,nodelist=p.keys(),
                           node_size=80,
                           node_color=p.values(),
                           cmap=cm.Reds_r)

    xlim(-0.05,1.05)
    ylim(-0.05,1.05)
    axis('off')
    savefig('random_geometric_graph.png')
    # show()

def save_graph(graph,file_name):
    #initialze Figure
    plt.figure(num=None, figsize=(25, 25), dpi=80)
    plt.axis('off')
    fig = plt.figure(1)
    nx.draw_spectral(graph, node_size=30)
    #pos = nx.spectral_layout(graph)
    #nx.draw_networkx_nodes(graph,pos, node_size=30)
    #nx.draw_networkx_edges(graph,pos)
    #nx.draw_networkx_labels(graph,pos)

    #cut = 1.00
    #xmax = cut * max(xx for xx, yy in pos.values())
    #ymax = cut * max(yy for xx, yy in pos.values())
    #plt.xlim(0, xmax)
    #plt.ylim(0, ymax)

    plt.savefig(file_name,bbox_inches="tight")
    pylab.close()
    del fig

def createGraph():
    # make a random graph of 500 nodes with expected degrees of 50
    n=15 # n nodes
    p=0.1
    #G = nx.fast_gnp_random_graph(n,p)
    G = nx.complete_graph(3)
    G = nx.fast_gnp_random_graph(4, 0.5)
    P=G
    P2=nx.strong_product(G,P)
    P3=nx.strong_product(G, P2)

    degreeList = list(P3.degree(P3.nodes()).values())

    nx.draw(G)
    plt.show()
    nx.draw_spectral(P3, cmap = plt.get_cmap('jet'), node_color = degreeList)
    plt.show()
    nx.draw_spring(P3, cmap = plt.get_cmap('jet'), node_color = degreeList)
    plt.show()
    #save_graph(G, "random_binomial_graph2.pdf")
    #save_graph(P2, "strong_product_random_binomial_graph2.pdf")
    #save_graph(P3, "strong_product_random_binomial_graph_cube.pdf")

    #nx.write_graphml(G, "G.graphml")
    #nx.write_graphml(P2, "P2.graphml")

    #nx.write_gexf(G, "G.gexf")
    # nx.write_gexf(P2, "P2.gexf")

def matplotlibTest():
    # Make a figure and axes with dimensions as desired.
    fig = plt.figure(figsize=(8,3))
    ax1 = fig.add_axes([0.05, 0.80, 0.9, 0.15])
    ax2 = fig.add_axes([0.05, 0.475, 0.9, 0.15])
    ax3 = fig.add_axes([0.05, 0.15, 0.9, 0.15])
    
    # Set the colormap and norm to correspond to the data for which
    # the colorbar will be used.
    cmap = mpl.cm.cool
    norm = mpl.colors.Normalize(vmin=5, vmax=10)
    
    # ColorbarBase derives from ScalarMappable and puts a colorbar
    # in a specified axes, so it has everything needed for a
    # standalone colorbar.  There are many more kwargs, but the
    # following gives a basic continuous colorbar with ticks
    # and labels.
    cb1 = mpl.colorbar.ColorbarBase(ax1, cmap=cmap,
                                       norm=norm,
                                       orientation='horizontal')
    cb1.set_label('Some Units')
    
    # The second example illustrates the use of a ListedColormap, a
    # BoundaryNorm, and extended ends to show the "over" and "under"
    # value colors.
    cmap = mpl.colors.ListedColormap(['r', 'g', 'b', 'c'])
    cmap.set_over('0.25')
    cmap.set_under('0.75')
    
    # If a ListedColormap is used, the length of the bounds array must be
    # one greater than the length of the color list.  The bounds must be
    # monotonically increasing.
    bounds = [1, 2, 4, 7, 8]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap,
                                         norm=norm,
                                         # to use 'extend', you must
                                         # specify two extra boundaries:
                                         boundaries=[0]+bounds+[13],
                                         extend='both',
                                         ticks=bounds, # optional
                                         spacing='proportional',
                                         orientation='horizontal')
    cb2.set_label('Discrete intervals, some other units')
    
    # The third example illustrates the use of custom length colorbar
    # extensions, used on a colorbar with discrete intervals.
    cmap = mpl.colors.ListedColormap([[0., .4, 1.], [0., .8, 1.],
        [1., .8, 0.], [1., .4, 0.]])
    cmap.set_over((1., 0., 0.))
    cmap.set_under((0., 0., 1.))
    
    bounds = [-1., -.5, 0., .5, 1.]
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    cb3 = mpl.colorbar.ColorbarBase(ax3, cmap=cmap,
                                         norm=norm,
                                         boundaries=[-10]+bounds+[10],
                                         extend='both',
                                         # Make the length of each extension
                                         # the same as the length of the
                                         # interior colors:
                                         extendfrac='auto',
                                         ticks=bounds,
                                         spacing='uniform',
                                         orientation='horizontal')
    cb3.set_label('Custom extension lengths, some other units')
    
    plt.show()

if __name__ == "__main__":
    main()