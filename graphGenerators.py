"""Ce module contient une banque de graphes construits '� la main' ainsi que quelques g�n�rateurs de graphes al�atoires.
Il y a aussi un morceau de fonction qui sert � remplacer les fichiers textes des graphes existants au cas ou il y aurait des modifications � faire."""
#     graphList = nx.graph_atlas_g()
#     graphList2 = [nx.petersen_graph(), nx.bull_graph(), nx.chvatal_graph(), nx.cubical_graph(), nx.desargues_graph(), nx.diamond_graph(), 
#                 nx.dodecahedral_graph(), nx.frucht_graph(), nx.heawood_graph(), nx.house_graph(), nx.house_x_graph(), nx.icosahedral_graph(), 
#                 nx.krackhardt_kite_graph(), nx.moebius_kantor_graph(), nx.octahedral_graph(), nx.pappus_graph(), nx.sedgewick_maze_graph(), 
#                 nx.tetrahedral_graph(), nx.truncated_cube_graph(), nx.truncated_tetrahedron_graph(), nx.tutte_graph()]
#          
#     graphList.remove(graphList[0])
#     for G in graphList:
#         if nx.is_connected(G):
#             try :
#                 src.writeGraph(G, path)
#             except Exception:
#                 pass
#     for G in graphList2:
#         if nx.is_connected(G):
#             try :
#                 src.writeGraph(G, path)
#             except Exception:
#                 pass

#     Random Generator 1
#     for j in range(10, 41):
#         if (j * 5) % 2 != 0 :
#             continue
#         print "Generating Random regular graph #" + str(j) + "..."
#         for i in range(0, 10000):
#     #         G = nx.gnp_random_graph(20, 0.50)
#     #         G = nx.gnm_random_graph(12, 18)
#             G = nx.random_regular_graph(4, 20)
#             if nx.is_connected(G):
#                 if not src.isCopWin(G, 1):
#                     if not src.isCopWin(G, 2):
#                         if not src.isCopWin(G, 3):
#                             G.name = "4-Regular graph #" + str(i)
#                             print "-------------------------------------"       
#                             src.writeGraph(G, path)
#                             print "-------------------------------------"    
    #         else:
    #             print "#" + str(i) + " n'est pas connexe."

    # Random Generator 2
#     for i in range(10, 31):
#         p = 0.05
#         for j in range(0, 1000):
#             print "Generating Random graph #" + str(i) + "-" + str(j) + " with a probability of " + str(p * 100) + "..."
#             G = nx.gnp_random_graph(i, p)
#             p += 0.00025
#             if nx.is_connected(G):
#                 G.name = "Random graph #" + str(i) + "-" + str(j)
#                 src.writeGraph(G, path)
#     G = nx.random_regular_graph(4, 10)
#     nx.draw(G)
#     plt.show()
#     print src.isCopWin(G, 2)

#     G = nx.Graph()
#     G.name = "Meta Pentagon Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 4), (4, 0)])
#     G.add_edges_from([(5, 6), (6, 7), (7, 8), (8, 9), (9, 5)])
#     G.add_edges_from([(10, 11), (11, 12), (12, 13), (13, 14), (14, 10)])
#     G.add_edges_from([(15, 16), (16, 17), (17, 18), (18, 19), (19, 15)])
#     G.add_edges_from([(20, 21), (21, 22), (22, 23), (23, 24), (24, 20)])
#     G.add_path([0, 5, 10, 15, 20, 0])
#     G.add_path([1, 6, 11, 16, 21, 1])
#     G.add_path([2, 7, 12, 17, 22, 2])
#     G.add_path([3, 8, 13, 18, 23, 3])
#     G.add_path([4, 9, 14, 19, 24, 4])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, path)
    
#     G = nx.Graph()
#     G.name = "Meta Square Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 0)])
#     G.add_edges_from([(5, 6), (6, 7), (7, 4), (4, 5)])
#     G.add_edges_from([(8, 9), (11, 8), (10, 11), (9, 10)])
#     G.add_edges_from([(12, 13), (13, 14), (14, 15), (15, 12)])
#     G.add_path([0, 4, 8, 12, 0])
#     G.add_path([1, 5, 9, 13, 1])
#     G.add_path([2, 6, 10, 14, 2])
#     G.add_path([3, 7, 11, 15, 3])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, path)
    
#     G = nx.Graph()
#     G.name = "3-Meta Square Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 0)])
#     G.add_edges_from([(5, 6), (6, 7), (7, 4), (4, 5)])
#     G.add_edges_from([(8, 9), (11, 8), (10, 11), (9, 10)])
#     G.add_path([0, 4, 8, 0])
#     G.add_path([1, 5, 9, 1])
#     G.add_path([2, 6, 10, 2])
#     G.add_path([3, 7, 11, 3])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, path)

#     G = nx.Graph()
#     G.name = "Meta Triangle Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8])
#     G.add_edges_from([(0, 1), (1, 2), (2, 0)])
#     G.add_edges_from([(3, 4), (4, 5), (5, 3)])
#     G.add_edges_from([(6, 7), (7, 8), (8, 6)])
#     G.add_path([0, 3, 6, 0])
#     G.add_path([1, 4, 7, 1])
#     G.add_path([2, 5, 8, 2])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, path)

#     G = nx.Graph()
#     G.name = "Odd graph O(4)"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34])
#     G.add_edges_from([(0, 17), (0, 8), (0, 34), (0, 1), (1, 2), (1, 13), (1, 28), (2, 3), (2, 32), (2, 24), (3, 4),
#                       (3, 16), (3, 21), (4, 5), (4, 26), (4, 9), (5, 6), (5, 13), (5, 18), (6, 7), (6, 29), (6, 33),
#                       (7, 8), (7, 24), (7, 15), (8, 9), (8, 20), (9, 10), (9, 31), (10, 11), (10, 28), (10, 23), (11, 12),
#                       (11, 33), (11, 16), (12, 13), (12, 20), (12, 25), (13, 14), (14, 15), (14, 31), (14, 22), (15, 16),
#                       (15, 27), (16, 17), (17, 18), (17, 30), (18, 19), (18, 23), (19, 20), (19, 32), (19, 27), (20, 21),
#                       (21, 22), (21, 29), (22, 23), (23, 24), (24, 25), (25, 26), (25, 30), (26, 27), (27, 28), (26, 34),
#                       (28, 29), (29, 30), (30, 31), (31, 32), (32, 33), (33, 34)])

#     G = nx.Graph()
#     G.name = "Meta Petersen Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])
#     G.add_path([0, 1, 2, 3, 4, 0])
#     G.add_path([5, 6, 7, 8, 9, 5])
#     G.add_path([10, 11, 12, 13, 14, 10])
#     G.add_path([15, 16, 17, 18, 19, 15])
#     pentagon1nodes = [0, 1, 2, 3, 4]
#     pentagon2nodes = [5, 6, 7, 8, 9]
#     pentagon3nodes = [10, 11, 12, 13, 14]
#     pentagon4nodes = [15, 16, 17, 18, 19]
#     G.add_edges_from([(0, 5), (1, 7), (2, 9), (3, 6), (4, 8), (5, 13), (6, 10), (7, 12), (8, 14), (9, 11), (10, 16), (11, 18), (12, 15), (13, 17), (14, 19), (15, 4), (16, 1), (17, 3), (18, 0), (19, 2)])

#     G = nx.Graph()
#     G.name = "Meta-Meta Petersen Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39])
#     G.add_path([0, 1, 2, 3, 4, 0])
#     G.add_path([5, 6, 7, 8, 9, 5])
#     G.add_path([10, 11, 12, 13, 14, 10])
#     G.add_path([15, 16, 17, 18, 19, 15])
#     G.add_path([20, 21, 22, 23, 24, 20])
#     G.add_path([25, 26, 27, 28, 29, 25])
#     G.add_path([30, 31, 32, 33, 34, 30])
#     G.add_path([35, 36, 37, 38, 39, 35])
#     pentagon1nodes = [0, 1, 2, 3, 4]
#     pentagon2nodes = [5, 6, 7, 8, 9]
#     pentagon3nodes = [10, 11, 12, 13, 14]
#     pentagon4nodes = [15, 16, 17, 18, 19]
#     pentagon5nodes = [20, 21, 22, 23, 24]
#     pentagon6nodes = [25, 26, 27, 28, 29]
#     pentagon7nodes = [30, 31, 32, 33, 34]
#     pentagon8nodes = [35, 36, 37, 38, 39]
#     G.add_edges_from([(0, 5), (1, 7), (2, 9), (3, 6), (4, 8), (5, 13), (6, 10), (7, 12), (8, 14), (9, 11), (10, 16), (11, 18), (12, 15), (13, 17), (14, 19), (15, 4), (16, 1), (17, 3), (18, 0), (19, 2)])
#     G.add_edges_from([(20, 25), (21, 27), (22, 29), (23, 26), (24, 28), (25, 33), (26, 30), (27, 32), (28, 34), (29, 31), (30, 36), (31, 38), (32, 35), (33, 37), (34, 39), (35, 24), (36, 21), (37, 23), (38, 20), (39, 22)])
#     G.add_edges_from([(0, 20), (1, 22), (2, 24), (3, 21), (4, 23), (5, 28), (6, 25), (7, 27), (8, 29), (9, 26), (10, 31), (11, 33), (12, 30), (13, 32), (14, 34), (15, 39), (16, 36), (17, 38), (18, 35), (19, 37)])

#     G = nx.Graph()
#     G.name = "Robertson Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18])
#     G.add_path([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0])
#     G.add_edges_from([(0, 4), (0, 14), (1, 9), (1, 12), (2, 6), (2, 17), (3, 11), (3, 15), (4, 8), (5, 12), (5, 16), (6, 10), (7, 15), (7, 18), (8, 13), (9, 16), (10, 14),
#                       (11, 18), (13, 17)])


#     G = nx.Graph()
#     G.name = "Szekeres Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (7, 9), (1, 9), (4, 9), (0, 5), (8, 3)])
#     G.add_edges_from([(10, 11), (11, 12), (12, 13), (13, 14), (14, 15), (15, 16), (16, 17), (17, 18), (17, 19), (11, 19), (14, 19), (10, 15), (18, 13)])
#     G.add_edges_from([(20, 21), (21, 22), (22, 23), (23, 24), (24, 25), (25, 26), (26, 27), (27, 28), (27, 29), (21, 29), (24, 29), (20, 25), (28, 23)])
#     G.add_edges_from([(30, 31), (31, 32), (32, 33), (33, 34), (34, 35), (35, 36), (36, 37), (37, 38), (37, 39), (31, 39), (34, 39), (30, 35), (38, 33)])
#     G.add_edges_from([(40, 41), (41, 42), (42, 43), (43, 44), (44, 45), (45, 46), (46, 47), (47, 48), (47, 49), (41, 49), (44, 49), (40, 45), (48, 43)])
#     G.add_edges_from([(0, 18), (40, 8), (48, 30), (38, 20), (28, 10)])
#     G.add_edges_from([(2, 36), (32, 16), (12, 46), (48, 26), (22, 6)])

#     analysisWindow = gui.AnalysisWindow(None, "title", "C:\Programmation\workspace Eclipse\GraphMaster9000\Old files")
#     for graph in analysisWindow.GetGraphList("C:\Programmation\workspace Eclipse\GraphMaster9000\Old files"):
#         G = src.loadGraph(src.readGraph("C:\Programmation\workspace Eclipse\GraphMaster9000\Old files\\" + graph)["title"], "C:\Programmation\workspace Eclipse\GraphMaster9000\Old files")
#         nx.write_graphml(G, path + "\\" + G.name + ".graphml")
#         actualGirth = src.getGirth(G)
#         actualMaxCycleLength = src.getMaxCycleLength(G)
#         
#         oldfile = open(oldpath)
#         newfile = open(path + "\\" + G.name + ".txt", "w")
#         
#         content = oldfile.read().splitlines()
# 
#         newfile.write(content[0] + "\n")
#         newfile.write(content[1] + "\n")
#         newfile.write(content[2] + "\n")
#         newfile.write(content[3] + "\n")
#         newfile.write(content[4] + "\n")
#         newfile.write(str(src.getGirth(G)) + "\n")
#         newfile.write(str(src.getMaxCycleLength(G)) + "\n")
#         newfile.write(content[7] + "\n")
#         newfile.write(content[8] + "\n")
#         newfile.write(content[9] + "\n")
#         newfile.write(content[10] + "\n")
#         newfile.write(content[11])
#         
#         oldfile.close()    
#         newfile.close()