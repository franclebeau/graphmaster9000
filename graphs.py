'''
Created on 2014-06-06

@author: franclebeau
'''
#     graphList = nx.graph_atlas_g()
#     graphList2 = [nx.petersen_graph(), nx.bull_graph(), nx.chvatal_graph(), nx.cubical_graph(), nx.desargues_graph(), nx.diamond_graph(), 
#                 nx.dodecahedral_graph(), nx.frucht_graph(), nx.heawood_graph(), nx.house_graph(), nx.house_x_graph(), nx.icosahedral_graph(), 
#                 nx.krackhardt_kite_graph(), nx.moebius_kantor_graph(), nx.octahedral_graph(), nx.pappus_graph(), nx.sedgewick_maze_graph(), 
#                 nx.tetrahedral_graph(), nx.truncated_cube_graph(), nx.truncated_tetrahedron_graph(), nx.tutte_graph()]
#          
#     graphList.remove(graphList[0])
#     for G in graphList:
#         if nx.is_connected(G):
#             try :
#                 src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")
#             except Exception:
#                 pass
#     for G in graphList2:
#         if nx.is_connected(G):
#             try :
#                 src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")
#             except Exception:
#                 pass

#     Random Generator
#     for j in range(10, 41):
#         if (j * 5) % 2 != 0 :
#             continue
#         print "Generating Random regular graph #" + str(j) + "..."
#         for i in range(0, 10000):
#     #         G = nx.gnp_random_graph(20, 0.50)
#     #         G = nx.gnm_random_graph(12, 18)
#             G = nx.random_regular_graph(4, 20)
#             if nx.is_connected(G):
#                 if not src.isCopWin(G, 1):
#                     if not src.isCopWin(G, 2):
#                         if not src.isCopWin(G, 3):
#                             G.name = "4-Regular graph #" + str(i)
#                             print "-------------------------------------"       
#                             src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")
#                             print "-------------------------------------"    
    #         else:
    #             print "#" + str(i) + " n'est pas connexe."

    # Random Generator
#     for i in range(10, 31):
#         p = 0.05
#         for j in range(0, 1000):
#             print "Generating Random graph #" + str(i) + "-" + str(j) + " with a probability of " + str(p * 100) + "..."
#             G = nx.gnp_random_graph(i, p)
#             p += 0.00025
#             if nx.is_connected(G):
#                 G.name = "Random graph #" + str(i) + "-" + str(j)
#                 src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")
#     G = nx.random_regular_graph(4, 10)
#     nx.draw(G)
#     plt.show()
#     print src.isCopWin(G, 2)

#     G = nx.Graph()
#     G.name = "Meta Petersen Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (4, 3), (0, 4), (5, 0), (6, 1), (7, 2), (8, 3), (9, 4), (8, 5), (5, 7), (9, 6), (8, 6), (9, 7)])
#     G.add_edges_from([(10, 11), (11, 12), (12, 13), (14, 13), (10, 14), (15, 10), (16, 11), (17, 12), (18, 13), (19, 14), (18, 15), (15, 17), (19, 16), (18, 16), (19, 17)])
#     G.add_edges_from([(20, 21), (21, 22), (22, 23), (24, 23), (20, 24), (25, 20), (26, 21), (27, 22), (28, 23), (29, 24), (28, 25), (25, 27), (29, 26), (28, 26), (29, 27)])
#     G.add_edges_from([(30, 31), (31, 32), (32, 33), (34, 33), (30, 34), (35, 30), (36, 31), (37, 32), (38, 33), (39, 34), (38, 35), (35, 37), (39, 36), (38, 36), (39, 37)])
#     G.add_path([0, 10, 20, 30, 0])
#     G.add_path([1, 11, 21, 31, 1])
#     G.add_path([2, 12, 22, 32, 2])
#     G.add_path([3, 13, 23, 33, 3])
#     G.add_path([4, 14, 24, 34, 4])
#     G.add_path([5, 15, 25, 35, 5])
#     G.add_path([6, 16, 26, 36, 6])
#     G.add_path([7, 17, 27, 37, 7])
#     G.add_path([8, 18, 28, 38, 8])
#     G.add_path([9, 19, 29, 39, 9])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")

#     G = nx.Graph()
#     G.name = "Smart Petersen Duo"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (4, 3), (0, 4), (5, 0), (6, 1), (7, 2), (8, 3), (9, 4), (8, 5), (5, 7), (9, 6), (8, 6), (9, 7)])
#     G.add_edges_from([(10, 11), (11, 12), (12, 13), (14, 13), (10, 14), (15, 10), (16, 11), (17, 12), (18, 13), (19, 14), (18, 15), (15, 17), (19, 16), (18, 16), (19, 17)])
#     G.add_edges_from([(0, 16), (1, 17), (2, 18), (3, 19), (4, 15), (5, 11), (6, 12), (7, 13), (8, 14), (9, 10)])
#     print src.isCopWin(G, 3)

#     G = nx.Graph()
#     G.name = "Meta Pentagon Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 4), (4, 0)])
#     G.add_edges_from([(5, 6), (6, 7), (7, 8), (8, 9), (9, 5)])
#     G.add_edges_from([(10, 11), (11, 12), (12, 13), (13, 14), (14, 10)])
#     G.add_edges_from([(15, 16), (16, 17), (17, 18), (18, 19), (19, 15)])
#     G.add_edges_from([(20, 21), (21, 22), (22, 23), (23, 24), (24, 20)])
#     G.add_path([0, 5, 10, 15, 20, 0])
#     G.add_path([1, 6, 11, 16, 21, 1])
#     G.add_path([2, 7, 12, 17, 22, 2])
#     G.add_path([3, 8, 13, 18, 23, 3])
#     G.add_path([4, 9, 14, 19, 24, 4])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")
    
#     G = nx.Graph()
#     G.name = "Meta Square Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 0)])
#     G.add_edges_from([(5, 6), (6, 7), (7, 4), (4, 5)])
#     G.add_edges_from([(8, 9), (11, 8), (10, 11), (9, 10)])
#     G.add_edges_from([(12, 13), (13, 14), (14, 15), (15, 12)])
#     G.add_path([0, 4, 8, 12, 0])
#     G.add_path([1, 5, 9, 13, 1])
#     G.add_path([2, 6, 10, 14, 2])
#     G.add_path([3, 7, 11, 15, 3])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")
    
#     G = nx.Graph()
#     G.name = "3-Meta Square Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 0)])
#     G.add_edges_from([(5, 6), (6, 7), (7, 4), (4, 5)])
#     G.add_edges_from([(8, 9), (11, 8), (10, 11), (9, 10)])
#     G.add_path([0, 4, 8, 0])
#     G.add_path([1, 5, 9, 1])
#     G.add_path([2, 6, 10, 2])
#     G.add_path([3, 7, 11, 3])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")

#     G = nx.Graph()
#     G.name = "Meta Triangle Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8])
#     G.add_edges_from([(0, 1), (1, 2), (2, 0)])
#     G.add_edges_from([(3, 4), (4, 5), (5, 3)])
#     G.add_edges_from([(6, 7), (7, 8), (8, 6)])
#     G.add_path([0, 3, 6, 0])
#     G.add_path([1, 4, 7, 1])
#     G.add_path([2, 5, 8, 2])
#     nx.draw(G)
#     plt.show()
#     src.writeGraph(G, "C:\Programmation\workspace Eclipse\GraphMaster9000\Graph files")

#     G = nx.Graph()
#     G.name = "Odd graph O(4)"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34])
#     G.add_edges_from([(0, 17), (0, 8), (0, 34), (0, 1), (1, 2), (1, 13), (1, 28), (2, 3), (2, 32), (2, 24), (3, 4),
#                       (3, 16), (3, 21), (4, 5), (4, 26), (4, 9), (5, 6), (5, 13), (5, 18), (6, 7), (6, 29), (6, 33),
#                       (7, 8), (7, 24), (7, 15), (8, 9), (8, 20), (9, 10), (9, 31), (10, 11), (10, 28), (10, 23), (11, 12),
#                       (11, 33), (11, 16), (12, 13), (12, 20), (12, 25), (13, 14), (14, 15), (14, 31), (14, 22), (15, 16),
#                       (15, 27), (16, 17), (17, 18), (17, 30), (18, 19), (18, 23), (19, 20), (19, 32), (19, 27), (20, 21),
#                       (21, 22), (21, 29), (22, 23), (23, 24), (24, 25), (25, 26), (25, 30), (26, 27), (27, 28), (26, 34),
#                       (28, 29), (29, 30), (30, 31), (31, 32), (32, 33), (33, 34)])

#     G = nx.Graph()
#     G.name = "Meta Petersen Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])
#     G.add_path([0, 1, 2, 3, 4, 0])
#     G.add_path([5, 6, 7, 8, 9, 5])
#     G.add_path([10, 11, 12, 13, 14, 10])
#     G.add_path([15, 16, 17, 18, 19, 15])
#     pentagon1nodes = [0, 1, 2, 3, 4]
#     pentagon2nodes = [5, 6, 7, 8, 9]
#     pentagon3nodes = [10, 11, 12, 13, 14]
#     pentagon4nodes = [15, 16, 17, 18, 19]
#     G.add_edges_from([(0, 5), (1, 7), (2, 9), (3, 6), (4, 8), (5, 13), (6, 10), (7, 12), (8, 14), (9, 11), (10, 16), (11, 18), (12, 15), (13, 17), (14, 19), (15, 4), (16, 1), (17, 3), (18, 0), (19, 2)])

#     G = nx.Graph()
#     G.name = "Meta-Meta Petersen Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39])
#     G.add_path([0, 1, 2, 3, 4, 0])
#     G.add_path([5, 6, 7, 8, 9, 5])
#     G.add_path([10, 11, 12, 13, 14, 10])
#     G.add_path([15, 16, 17, 18, 19, 15])
#     G.add_path([20, 21, 22, 23, 24, 20])
#     G.add_path([25, 26, 27, 28, 29, 25])
#     G.add_path([30, 31, 32, 33, 34, 30])
#     G.add_path([35, 36, 37, 38, 39, 35])
#     pentagon1nodes = [0, 1, 2, 3, 4]
#     pentagon2nodes = [5, 6, 7, 8, 9]
#     pentagon3nodes = [10, 11, 12, 13, 14]
#     pentagon4nodes = [15, 16, 17, 18, 19]
#     pentagon5nodes = [20, 21, 22, 23, 24]
#     pentagon6nodes = [25, 26, 27, 28, 29]
#     pentagon7nodes = [30, 31, 32, 33, 34]
#     pentagon8nodes = [35, 36, 37, 38, 39]
#     G.add_edges_from([(0, 5), (1, 7), (2, 9), (3, 6), (4, 8), (5, 13), (6, 10), (7, 12), (8, 14), (9, 11), (10, 16), (11, 18), (12, 15), (13, 17), (14, 19), (15, 4), (16, 1), (17, 3), (18, 0), (19, 2)])
#     G.add_edges_from([(20, 25), (21, 27), (22, 29), (23, 26), (24, 28), (25, 33), (26, 30), (27, 32), (28, 34), (29, 31), (30, 36), (31, 38), (32, 35), (33, 37), (34, 39), (35, 24), (36, 21), (37, 23), (38, 20), (39, 22)])
#     G.add_edges_from([(0, 20), (1, 22), (2, 24), (3, 21), (4, 23), (5, 28), (6, 25), (7, 27), (8, 29), (9, 26), (10, 31), (11, 33), (12, 30), (13, 32), (14, 34), (15, 39), (16, 36), (17, 38), (18, 35), (19, 37)])

#     G = nx.Graph()
#     G.name = "Robertson Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18])
#     G.add_path([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0])
#     G.add_edges_from([(0, 4), (0, 14), (1, 9), (1, 12), (2, 6), (2, 17), (3, 11), (3, 15), (4, 8), (5, 12), (5, 16), (6, 10), (7, 15), (7, 18), (8, 13), (9, 16), (10, 14),
#                       (11, 18), (13, 17)])

#     edges = [[pentagon1nodes, pentagon5nodes], [pentagon2nodes, pentagon6nodes], [pentagon3nodes, pentagon7nodes], [pentagon4nodes, pentagon8nodes]]
#     edges2 = [[pentagon1nodes, pentagon2nodes]]
#     edges3 = [[pentagon1nodes, pentagon2nodes], [pentagon2nodes, pentagon3nodes], [pentagon3nodes, pentagon4nodes], [pentagon4nodes, pentagon1nodes]]
#     edges4 = [pentagon4nodes, pentagon1nodes]

#     choiceList = src.getChoiceList(edges3, pentagon1nodes)       
#     configList = src.getConfigList(choiceList, pentagon1nodes)
#     for config in configList:
#         print config
#     for config in src.getSmartConfig(G, configList):
#         print config

#     G = nx.Graph()
#     G.name = "Szekeres Graph"
#     G.add_nodes_from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49])
#     G.add_edges_from([(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (7, 9), (1, 9), (4, 9), (0, 5), (8, 3)])
#     G.add_edges_from([(10, 11), (11, 12), (12, 13), (13, 14), (14, 15), (15, 16), (16, 17), (17, 18), (17, 19), (11, 19), (14, 19), (10, 15), (18, 13)])
#     G.add_edges_from([(20, 21), (21, 22), (22, 23), (23, 24), (24, 25), (25, 26), (26, 27), (27, 28), (27, 29), (21, 29), (24, 29), (20, 25), (28, 23)])
#     G.add_edges_from([(30, 31), (31, 32), (32, 33), (33, 34), (34, 35), (35, 36), (36, 37), (37, 38), (37, 39), (31, 39), (34, 39), (30, 35), (38, 33)])
#     G.add_edges_from([(40, 41), (41, 42), (42, 43), (43, 44), (44, 45), (45, 46), (46, 47), (47, 48), (47, 49), (41, 49), (44, 49), (40, 45), (48, 43)])
#     G.add_edges_from([(0, 18), (40, 8), (48, 30), (38, 20), (28, 10)])
#     G.add_edges_from([(2, 36), (32, 16), (12, 46), (48, 26), (22, 6)])
    
#===============================================================================
#     K_10 = nx.complete_graph(10)     
#            
#     petersen = nx.petersen_graph()
#     
#     C_4 = nx.Graph()
#     C_4.add_edges_from([(0,1),(1,2), (2,3), (0,3)])
#     
#     P_100 = nx.path_graph(100)
#     
#     graphDiagonales = nx.Graph()
#     graphDiagonales.add_edges_from([(11, 6), (11, 7), (11, 8), (11, 9), (11, 10)])     
#     graphDiagonales.add_edges_from([(6, 7), (7, 8), (8, 9), (9, 10)])      
#     graphDiagonales.add_edges_from([(6, 1), (7, 2), (8, 3), (9, 4), (10, 5)]) 
#     graphDiagonales.add_edges_from([(1, 7), (2, 8), (3, 9), (4, 10)])
#     graphDiagonales.add_edges_from([(2, 1), (3, 2), (4, 3), (5, 4)])
#     
#     tutte = nx.tutte_graph()
#      
#     petersen2 = nx.petersen_graph()
#     petersen2.remove_edge(5, 7)    
#     petersen2.remove_edge(4, 9)
#     petersen2.remove_edge(3, 8)    
#     petersen2.add_path([4, 16, 17, 18, 9])
#     petersen2.add_path([3, 13, 14, 15, 8])
#     petersen2.add_path([5, 10, 11, 12, 7])
#     
#     
#     for node in K_10:
#         K_10.add_edge(node, node)
#     for node in petersen:
#         petersen.add_edge(node, node)
#     for node in C_4:
#         C_4.add_edge(node, node)
#     for node in P_100:
#         P_100.add_edge(node, node)
#     for node in graphDiagonales:
#         graphDiagonales.add_edge(node, node)
#     for node in tutte:
#         tutte.add_edge(node, node)
#     for node in petersen2:
#         petersen2.add_edge(node, node)
#     
#     assert src.isCopWin(K_10, 1)                     # expected : True
#     assert src.isCopWin(K_10, 2)                     # expected : True
#     assert not src.isCopWin(petersen, 2)             # expected : False
#     assert not src.isCopWin(C_4, 1)                  # expected : False
#     assert src.isCopWin(P_100, 1)                    # expected : True
#     assert src.isCopWin(graphDiagonales, 1)          # expected : True
#     assert src.isCopWin(C_4, 2)                      # expected : True
#     assert src.isCopWin(petersen, 3)                 # expected : True
#     assert not src.isCopWin(tutte, 2)                # expected : False
# #     print src.isCopWin(tutte, 3)                     # expected : Trop long
#     assert src.isCopWin(petersen2, 2)                # expected : True
#     nx.draw(petersen)
#     plt.show()
#===============================================================================
