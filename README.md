# README #

Le GraphMaster9000 est un logiciel d'étude des graphes, de leurs propriétés ainsi que de leur relation au jeu de poursuite de type policier-voleur.

En utilisant les fonctions de la librairie networkX, l'utilisateur peut créer le graphe de son choix. Ce graphe peut ensuite être sauvegardé à l'aide
de la fonction src.writeGraph et comparé avec les autres graphes sauvegardés. Note : si l'utilisateur créé de nouveaux graphes, il devra ajouter lui-même à la fin
du fichier texte le temps de capture à partir du dictRelation de src.plusPetitEgalMou. La raison en est que le temps de capture n'est pas utilisé dans le logiciel,
il a été ajouté seulement aux fichiers textes existants à des fins d'observation.

Afin de déterminer si un graphe G est k-copwin ou non, utiliser la fonction src.isCopwin(G, k). Afin de trouver directement le copnumber d'un graphe G,
utiliser la fonction src.getCopNumber(G). Attention : les graphes de 40 sommets et plus vont prendre un très grand temps à analyser pour k = 3. La fonction étant
coûteuse en mémoire, tout graphe de plus de 50 sommets fera planter la plupart des ordinateurs pour k >= 3.

Les graphes qui ne sont pas dans le dossier "Graph files" sont des graphes qui sont trop volumineux pour être analysés rapidement, ils ont donc été mis à part.

Note : afin de pouvoir analyser un graphe à l'aide de l'interface, il doit y avoir dans le path choisit un fichier .graphml et un fichier .txt du graphe. La fonction src.writeGraph s'occupe d'écrire ces deux fichiers.

Note : si l'utilisateur veut utiliser des graphes qui n'ont pas de temps de capture dans leur fichier texte, s'assurer de mettre en commentaire la ligne qui lit les temps de capture dans la fonction src.readGraph. 

Auteur : François Lebeau

francois.lebeau.2@ulaval.ca

Université Laval
Département d'informatique et de génie logiciel