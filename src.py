# -*- coding:Utf8 -*-

import networkx as nx
import os
import copy
import itertools
from itertools import product


def getCopNumber(G):
    """Fonction mère du cop-number. Détermine le cop-number d'un graphe à l'aide de la relation plusPetitEgalMou."""
    found = False
    k = 0
    while not found:
        k += 1
#         print "Testing " + str(k) + "-copwin for " + G.name    # Utile pour garder le fil lorsqu'on teste plusieurs graphes d'affilé
        if isCopWin(G, k):
            found = True
    return str(k)    
    
def isCopWin(G, k): 
    """Fonction principale du cop-number. Détermine le cop-number d'un graphe à l'aide de la relation plusPetitEgalMou."""
    graph = G.copy()                            # Mettre ces lignes en commentaire
    for node in graph:                          # pour jouer sur des
        graph.add_edge(node, node)              # graphes non-réflexifs
    dictRelation = getDictRelation(graph, k)
    winningPositions = []
    for key in dictRelation:
        if key[0] in key[1]:
            """Si un policier est à la même position que le voleur"""
            dictRelation[key] = 0            
        else:
            for cops in key[1]:
                if nx.shortest_path_length(graph, key[0], cops) == 1:
                    """Si un des policiers est à distance 1 du voleur"""
                    dictRelation[key] = 1
                    break
            
    plusPetitEgalMou(graph, dictRelation, 2, winningPositions)

    if None in dictRelation.values():    
        return False
    else:        
        return True
    
def getDictRelation(G, k):
    """Retourne l'ensemble d'états possible du jeu."""
    dictRelation = {}
    copMoveList = getCopMoveList(G, k)
    for node in G:
        for move in copMoveList:
            """On stock chaque paire de sommets dans un dictionnaire. Cette paire de sommets sera matchée 
            à un entier n si cet état de jeu mène à une capture en n coups"""
            dictRelation[(node, move)] = None
    return dictRelation
    
def getCopMoveList(G, k):
    moveList = []
    listsOfNodes = [] 
    numberOfNodes = len(nx.nodes(G))
    if k > numberOfNodes:
        raise Exception
    while k > 0:
        listsOfNodes.append(nx.nodes(G))  
        k -= 1
    for tuples in product(*listsOfNodes):
        noDoubles = True
        for nodes in tuples:
            if tuples.count(nodes) > 1:
                noDoubles = False
        if noDoubles:
            moveList.append(tuples)
    return moveList

def kStrongProduct(G, k):
    """Sert à obtenir le k-produit fort d'un graphe G."""
    count = k
    P = nx.strong_product(G, G)
    count -= 1
    while count > 1:
        P = nx.strong_product(P, G)
        count -= 1        
    return P
            
def plusPetitEgalMou(G, dictRelation, n, winningPositions):    
#     print n     # Utile pour savoir à quelle vitesse la fonction avance
    for key in dictRelation:
        if dictRelation[key] == None:
            if (key[0], set(key[1])) in winningPositions:
                """La liste winningPositions sert à optimiser la fonction : l'état de jeu (2, (5, 4, 3)) est équivalent à (2, (3, 5, 4)) et en est traité ainsi."""
                dictRelation[key] = n
                continue
            """Mouvements possibles du/des policiers."""
            possibleMoveList = getPossibleMoveList(G, key[1])
            for copMove in possibleMoveList:
                
                """Si le policier ne bouge pas, ça ne sert à rien."""
                if set(copMove) == set(key[1]):
                    continue       
                
                """On cherche un mouvement du voleur qui peut le garder en liberté plus longtemps."""
                found = False
                robberMoveList = nx.all_neighbors(G, key[0])
                for robberMove in robberMoveList:
                    if dictRelation[(robberMove, copMove)] >= n or dictRelation[(robberMove, copMove)] == None:
                        found = True                        
                        break
                """S'il n'y en a pas, alors c'est une capture en n coups."""
                if not found:
                    dictRelation[key] = n
                    """Il en est de même pour les positions de policiers équivalentes."""
                    winningPositions.append((key[0], set(key[1])))
                    break
    """Si on a trouvé une capture en n coups, il existe peut-être une capture en n + 1 coups."""    
    if n in dictRelation.values():
        plusPetitEgalMou(G, dictRelation, n + 1, winningPositions)
        
def getPossibleMoveList(G, position):
    """Donne les mouvements possibles des k policiers."""
    moveList = []
    neighborList = []
    setList = []
    for cops in position:
        neighborList.append(nx.all_neighbors(G, cops))
    for tuples in product(*neighborList):
        noDoubles = True
        for nodes in tuples:
            if tuples.count(nodes) > 1:
                noDoubles = False
        if noDoubles:
            if set(tuples) not in setList:
                setList.append(set(tuples)) 
                moveList.append(tuples)   
    return moveList

def findGraph(graphName, path):
    for files in os.walk(path):
        if graphName in files[2]:
            return True
        
def writeGraph(G, path):
    """Écrit le fichier .txt d'un graphe pour être lu plus tard, ainsi que le fichier .graphml, à condition que le fichier en question n'existe pas déjà."""
    if not findGraph(G.name + ".graphml", path):
        print "Writing " + G.name + "..."
        nx.write_graphml(G, path + "\\" + G.name + ".graphml")
        f = open(path + "\\" + G.name + ".txt", "w")
        f.write(G.name + "\n")
        f.write(str(nx.number_of_nodes(G)) + "\n")
        f.write(str(nx.diameter(G)) + "\n")
        f.write(str(nx.radius(G)) + "\n")
        f.write(str(len(nx.cycle_basis(G))) + "\n")
        f.write(str(getGirth(G)) + "\n")
        f.write(str(getMaxCycleLength(G)) + "\n")
        f.write(str(int(nx.density(G) * 10)) + "\n")
        f.write(str(min(nx.degree(G).values())) + "\n")
        f.write(str(max(nx.degree(G).values())) + "\n")
        f.write(str(getCopNumber(G)))
        
        
        print G.name + " has been succesfully written."
        f.close()
    else:
        raise Exception
def loadGraph(graphName, path):
    return nx.read_graphml(path + "\\" + graphName + ".graphml") 
def readGraph(path):
    graphDict = {}
    with open(path) as f:
        content = f.read().splitlines()
        graphDict["title"] = content[0]
        graphDict["nodes"] = int(content[1])
        graphDict["diameter"] = int(content[2])
        graphDict["radius"] = int(content[3])
        graphDict["cycles"] = int(content[4])
        graphDict["mincyclelength"] = int(content[5])
        graphDict["maxcyclelength"] = int(content[6])
        graphDict["density"] = int(content[7])
        graphDict["mindegree"] = int(content[8])
        graphDict["maxdegree"] = int(content[9])
        graphDict["copwin"] = int(content[10])
        graphDict["capture"] = int(content[11])        # À utiliser seulement si le fichier texte comporte le temps de capture optimal en fin de fichier
    return graphDict
def graphFilter(listToFilter, filterDict, path):
    """Filtre la liste de graphes dans l'interface Graph Analysis selon les contraintes choisies."""
    filteredList = copy.copy(listToFilter)
    for key in filterDict.keys():
        for graph in listToFilter:
            if not relationTranslation(filterDict[key][0], filterDict[key][1], readGraph(path + '\\' + graph)[key]):
                try:
                    filteredList.remove(graph)
                except:
                    continue    
    return filteredList   
def relationTranslation(relationString, constraint, actual):
    """Sert à transformer les strings de relations dans l'interface de Graph Analysis en relations logiques."""
    if relationString == "<":
        return actual < constraint
    if relationString == "<=":
        return actual <= constraint
    if relationString == "==":
        return actual == constraint
    if relationString == ">=":
        return actual >= constraint
    if relationString == ">":
        return actual > constraint
    if relationString == "!=":
        return actual != constraint
    
def getGirth(G):
    """Retourne la maille d'un graphe G."""
    girth = len(nx.nodes(G))
    for edge in nx.edges(G):
        G.remove_edge(edge[0], edge[1])
        try:
            distance = nx.shortest_path_length(G, edge[0], edge[1])
        except:
            distance = len(nx.nodes(G)) + 2
        if girth > distance + 1:
            girth = distance + 1
        G.add_edge(edge[0], edge[1])
    return girth

def getMaxCycleLength(G):
    """Retourne le plus long cycle minimal entre 2 points quelconque."""
    maxCycleLength = 0
    for edge in nx.edges(G):
        G.remove_edge(edge[0], edge[1])
        try:
            distance = nx.shortest_path_length(G, edge[0], edge[1])
        except:
            distance = 0
        if maxCycleLength < distance + 1:
            maxCycleLength = distance + 1
        G.add_edge(edge[0], edge[1])
    return maxCycleLength


def playGame(G):
    """Cette fonction simule le jeu de poursuite et calcule le temps de capture maximal selon la stratégie définie par la fonction executeStrategy.
    L'objectif était de trouver une façon de coder une stratégie optimal qui retournerait le temps de capture optimal, peu importe le graphe.
    Le code tel qu'il est au moment de l'écriture de ce commentaire n'est pas concluant, il semblerait que l'approche visant à réduire naïvement la
    distance totale entre les policiers et les voisins du voleur n'est pas suffisament efficace."""
    k = max(nx.degree(G).values())
    
    graph = G.copy()   
    for node in graph:
        graph.add_edge(node, node)   
    
    dictRelation = getDictRelation(graph, k)
    for key in dictRelation:
        relevant = True
        if key[0] in key[1]:
            """Si un policier est à la même position que le voleur"""
            continue
        for cops in key[1]:
            if nx.shortest_path_length(graph, key[0], cops) == 1:
                """Si un des policiers est à distance 1 du voleur"""
                relevant = False
                break
        if not relevant:
            continue
        else:    
            dictRelation[key] = executeStrategy(graph, key, 0)
    """L'idée était de comparer le temps de capture maximum dérivé de la stratégie codée et le comparer avec le temps de capture optimal donné par le plusPetitEgalMou"""
    return max(dictRelation.values())

def executeStrategy(G, gameState, count):
    """Count : le nombre de tours joués par les policiers""" 
    maxCount = count
    if neighborhoodInclusion(G, gameState):
        return maxCount
                    
    maxCount += 1
    copMoves = findMinimalDistance(G, gameState)
    
    gameState = (gameState[0], tuple(copMoves))
    
    if neighborhoodInclusion(G, gameState):
        return maxCount
    
    robberMoves = getSmartMoves(G, gameState)
    
    if len(robberMoves) == 0:
        return maxCount
    
    newGameStates = []
    for i in range(0, len(robberMoves)):
        newGameStates.append((robberMoves[i], gameState[1]))
        
    count = maxCount        
    
    for gameStates in newGameStates:
        newCount = executeStrategy(G, gameStates, count)
        if newCount > maxCount:
            maxCount = newCount
        
    return maxCount

def getSmartMoves(G, key):
    """Cette fonction sert à trier les mouvements du voleur de manière à sélectionner uniquement les mouvements intelligents."""
    robberMoves = []
    for neighbors in nx.all_neighbors(G, key[0]):
        robberMoves.append(neighbors)
    
    smartMoves = []
    
    for moves in robberMoves: 
        relevant = True
        if moves == key[0]:
            """Si le voleur ne bouge pas, il ne perd rien pour attendre"""
            continue   
        if moves in key[1]:
                """Si un policier est à la même position que le voleur"""
                continue
        for cops in key[1]:            
            if nx.shortest_path_length(G, moves, cops) == 1:
                """Si un des policiers est à distance 1 du voleur"""
                relevant = False
                break
        if relevant:
            smartMoves.append(moves)    
    
    return smartMoves       

def neighborhoodInclusion(G, key):
    """Vérifie si le voisinage du voleur est couvert par les k-policiers."""
    inclusionList = []
    for neighbors in nx.all_neighbors(G, key[0]):
        inclusionList.append(neighbors)
        
    copRange = []
    for i in range(0, len(key[1])):
        for neighbor in nx.all_neighbors(G, key[1][i]):
            copRange.append(neighbor)
    
    if set(inclusionList).issubset(set(copRange)):
        return True
    else:
        return False
    
def findMinimalDistance(G, key):
    """Le coeur de la stratégie : à chaque tour, on trouve l'ensemble de chemin des k-policiers aux k voisins du voleur tel que la distance totale des
    k chemins est minimale."""
    targets = []
    for neighbor in nx.all_neighbors(G, key[0]):
        if neighbor != key[0]:
            targets.append(neighbor)
    
    totalDistance = len(nx.nodes(G)) * len(targets)
    permutations = []
    for items in itertools.permutations(key[1]):
        permutations.append(items)
    for candidate in permutations:
        distance = 0
        for i in range(0, len(candidate)):
            G.remove_node(key[0])
            distance += nx.shortest_path_length(G, candidate[i], targets[i])            
            G.add_node(key[0])
            for node in targets:
                G.add_edges_from([(key[0], node)])
        if distance < totalDistance:
            totalDistance = distance
            chosenPaths = candidate
            
    moves = []
    for j in range(0, len(chosenPaths)):
        pathway = nx.shortest_path(G, chosenPaths[j], targets[j])
        if len(pathway) > 1:
            if pathway[1] not in moves:
                moves.append(pathway[1])
            else:
                moves.append(pathway[0])
        else:
            moves.append(pathway[0])
    return moves   
                             