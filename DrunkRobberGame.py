# -*- coding:Utf8 -*-

__author__ = 'Frédéric'

import itertools

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


def main( ) :
    # numNodes = 10
    #G = nx.cycle_graph(numNodes)

    #print("L'esperance du temps de capture sur le graphe G est : " +str(expectedCaptureTime(G, 0.00005)))
    #print("L'esperance du temps de cpature avec deux policiers est : " + str(expectedCaptureTimeWithKCops(G, 0.005, 2)))
    #print("L'esperance du temps de cpature avec trois policiers est : " + str(expectedCaptureTimeWithKCops(G, 0.05, 3)))

    d = 2
    h = 4
    d_prime = 2
    h_prime = 3
    G = nx.balanced_tree(d, h)
    G_prime = nx.balanced_tree(d_prime, h_prime)
    H = nx.path_graph(h)

    #createAndShowStrongProduct(G,H)
    # createAndShowStrongProduct(G,H, (8,6))

    # T1 = nx.balanced_tree(d, h)
    # T2 = nx.balanced_tree(d + 1, h - 1)
    #createAndShowStrongProduct(T1, T2, (8, 6))

    #T = nx.balanced_tree(d,h)
    #C = nx.cycle_graph(5)
    #createAndShowStrongProduct(T,C,(8,6))


def playGameAndShowResults( G ) :
    n = 0
    W = np.zeros((1000, G.order(), G.order()))
    proba = wn(n, G, W)
    while (proba < 1) :
        n += 1
        W = np.zeros((1000, G.order(), G.order()))
        proba = wn(n, G, W)

    return n


def wnXYk( x, y, n, G, P, W, moves ) :
    if (n == 0) :
        if (x in y) :
            return 1
        else :
            return 0

    else :
        numberOfNodeY = nx.get_node_attributes(P, "node numbers")[y]
        if (W[n, x, numberOfNodeY] > 0) :
            return W[n, x, numberOfNodeY]

        maximum = 0
        for y_prime in closeNeighborhoodForKCops(y, G, moves) :
            summation = 0
            for x_prime in closedNeighborhood(x, G) :
                proba = wnXYk(x_prime, y_prime, n - 1, G, P, W, moves)
                summation += (1 / len(closedNeighborhood(x, G))) * proba
            if (maximum < summation) :
                maximum = summation
        W[n, x, numberOfNodeY] = maximum
        if (maximum > 1) :
            raise Exception("Probabilite superieure a 1")
        return maximum


# Pour l'instant on suppose une marche aléatoire uniforme
def wnXY( x, y, n, G, W, nextMove ) :
    if (n == 0) :
        if (x == y) :
            return 1, y
        else :
            return 0, y
    else :
        if (W[n, x, y] > 0) :
            return W[n, x, y], y

        max_w = 0
        nextMove = 0
        neighbors = closedNeighborhood(x, G)
        if y in neighbors :
            return 1
        else :
            for y_prime in closedNeighborhood(y, G) :
                sum_w = 0
                for x_prime in closedNeighborhood(x, G) :
                    proba, nextMove = wnXY(x_prime, y_prime, n - 1, G, W, nextMove)
                    sum_w += (1 / len(closedNeighborhood(x, G))) * proba
                if (max_w < sum_w) :
                    max_w = sum_w
                    nextMove = y_prime
            W[n, x, y] = max_w
            return max_w, nextMove


def wn( n, G, W ) :
    sum = 0
    for y in G.nodes() :
        for x in G.nodes() :
            sum += wnXY(x, y, n, G, W, 0)[0]
    return sum


def closedNeighborhood( x, G ) :
    x_prime_list = G.neighbors(x)
    x_prime_list.append(x)
    return x_prime_list


def closeNeighborhoodForKCops( x, G, possibleMoves ) :
    x_prime_list = []
    for moves in possibleMoves :
        for v in x :
            for pos in moves :
                if ((pos in G.neighbors(v) or pos == v)) :
                    x_prime_list.append(moves)
    x_prime_list.append(x)
    x_prime_list = set(x_prime_list)
    return x_prime_list


def expectedCaptureTimeGivenXY( G, x, y, epsilon, W ) :
    n = 0
    wN = wnXY(x, y, n, G, W)
    partialExpectation = 0
    upperbound = (4 * G.order() ** 3) / 27
    while wN > epsilon and n < upperbound :
        n += 1
        wN = wnXY(x, y, n, G, W) * (1 - W[n - 1, x, y])
        partialExpectation += 1 - wN
    return partialExpectation


def expectedCaptureTimeGivenXYAndKCops( G, P, x, y, epsilon, W, moves ) :
    n = 0
    wNK = wnXYk(x, y, n, G, P, W, moves)
    partialExp = 0
    upperBound = 0
    for z in y :
        upperBound += hittingTimeBound(z, G)
    while wNK > epsilon and n < upperBound :
        n += 1
        wNK = wnXYk(x, y, n, G, P, W, moves) * (1 - W[n - 1, x, nx.get_node_attributes(P, "node numbers")[y]])
        partialExp += n * wNK
    return partialExp


def expectedCaptureTime( G, epsilon ) :
    expectation = 0
    W = np.zeros((1000, G.order(), G.order()))
    for x in G.nodes() :
        for y in G.nodes() :
            expectation += expectedCaptureTimeGivenXY(G, x, y, epsilon, W)
    expectation /= (G.order() ** 2)

    return expectation


def expectedCaptureTimeWithKCops( G, epsilon, k ) :
    expectation = 0
    P = computeStrongProduct(G, k)
    W = np.zeros((1000, G.order(), P.order()))
    possibleMovesForKCops = computePossiblePermutationsOfKCops(G, k)
    for x in G.nodes() :
        for y in possibleMovesForKCops :
            expectation += expectedCaptureTimeGivenXYAndKCops(G, P, x, y, epsilon, W, possibleMovesForKCops)
    expectation /= G.order() * len(possibleMovesForKCops)
    return expectation


def markovMatrixForHitXY( G, x, y ) :
    hitVertex = (x, y)
    P = np.zeros((G.order(), G.order()))
    for x_prime in G.nodes() :
        for y_prime in G.nodes() :
            if (y_prime in G.neighbors(x_prime)) :
                P[x_prime, y_prime] = 1
    P += np.transpose(P)
    P[hitVertex[1], :] = 0
    P[hitVertex[1], hitVertex[1]] = 1
    P = np.transpose(np.transpose(P) / np.sum(P, axis=1))
    return P


def computeStrongProduct( G, k ) :
    P = G
    for i in range(1, k) :
        P = nx.strong_product(P, G)
    n = 0
    nodeNumbers = { }
    for x in P.nodes() :
        nodeNumbers[x] = n
        n += 1
    nx.set_node_attributes(P, "node numbers", nodeNumbers)
    return P


def computePossiblePermutationsOfKCops( G, k ) :
    permutations = []
    for subsets in itertools.combinations(G.nodes(), k) :
        if (len(set(subsets)) == k) :
            permutations.append(subsets)
    return permutations


def hittingTimeBound( y, G ) :
    return 2 * G.size() - G.degree(y)


def createAndShowStrongProduct( G, H, size ) :
    if (size == { }) :
        size = (8, 6)

    P = nx.strong_product(G, H)
    degreeList = list(P.degree(P.nodes()).values())
    d = nx.degree(P)
    print(d)

    plt.figure(1, figsize=size, dpi=80)
    nx.draw_spectral(P, cmap=plt.get_cmap('jet'), node_color=degreeList, node_size=[v * 100 for v in d.values()])
    plt.figure(2, figsize=size, dpi=80)
    nx.draw_spring(P, cmap=plt.get_cmap('jet'), node_color=degreeList, node_size=[v * 100 for v in d.values()])
    plt.show()


if __name__ == '__main__' :
    main()