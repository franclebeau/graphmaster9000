# -*- coding:Utf8 -*-

__author__ = 'Frédéric'

import networkx as nx
from matplotlib import pyplot as plt


def main( ) :
    # G = nx.fast_gnp_random_graph(30, 0.70)
    # drawGraphOnCentrality(G)

    #G = nx.connected_watts_strogatz_graph(30, 7, 0.70)
    #drawGraphOnCentrality(G)

    i = 0
    G = nx.random_geometric_graph(20, 0.120)
    while (nx.is_connected(G) == False) :
        G = nx.random_geometric_graph(20, 0.120 + 0.1 * i)
        i += 1
    print(i)
    drawGraphOnCentrality(G)


def computeBetweenness( G, weight ) :
    if ( len(weight) == 0) :
        nodeCentrality = nx.current_flow_betweenness_centrality(G)
    else :
        nodeCentrality = nx.current_flow_betweenness_centrality(G, weight)

    return nodeCentrality


def findMaxCentrality( nodeCentrality ) :
    maxCentralNode = max(nodeCentrality, key=nodeCentrality.get)
    maxCentralValue = nodeCentrality[maxCentralNode]
    return maxCentralNode, maxCentralValue


def drawGraphOnCentrality( G ) :
    cmapName = "Blues"
    nodeCentrality = computeBetweenness(G, { })
    centralNode, centralValue = findMaxCentrality(nodeCentrality)
    sortedListOfCentrals = sorted(nodeCentrality, key=nodeCentrality.get)
    val_map = { }
    for y in G.nodes() :
        if (nodeCentrality[y] == centralValue) :
            val_map[y] = centralValue
    values = [nodeCentrality.get(node, 0.0) for node in G.nodes()]

    roundedCentrality = { }
    for node in nodeCentrality :
        roundedCentrality[node] = round(nodeCentrality[node], 2)

    nx.draw_spectral(G, cmap=plt.get_cmap(cmapName), node_color=values, labels=roundedCentrality)
    plt.show()
    nx.draw_spring(G, cmap=plt.get_cmap(cmapName), node_color=values, labels=roundedCentrality)
    plt.show()


if __name__ == "__main__" :
    main()