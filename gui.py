
import wx
import src
import os
import networkx as nx
import copy
import matplotlib.pyplot as plt
from wx.lib.wordwrap import wordwrap


class MainWindow(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(312,500))
        panel = wx.Panel(self)
        self.CreateStatusBar() 
        self.path = None
        
        # # # Menu # # #    
        filemenu= wx.Menu()
        menuPath = filemenu.Append(wx.ID_ANY, "Choose Path", "Choose working directory")
        menuAbout = filemenu.Append(wx.ID_ABOUT, "&About","Information about this program")
        filemenu.AppendSeparator()
        menuExit = filemenu.Append(wx.ID_EXIT,"E&xit","Terminate the program")
        
        self.Bind(wx.EVT_MENU, self.OnPath, menuPath)
        menuBar = wx.MenuBar()        
        menuBar.Append(filemenu,"&File")
        self.SetMenuBar(menuBar)
        # # # 
        
        # # # Buttons # # #
        editorButton = wx.Button(panel, wx.ID_ANY, "Graph Editor", (50, 50))
        self.Bind(wx.EVT_BUTTON, self.OnClickEditor, editorButton)
        editorButton.SetSize((200, 100))
        editorButton.SetToolTipString("Open the Graph Editor interface.")
        
        analysisButton = wx.Button(panel, wx.ID_ANY, "Graph Analysis", (50, 250))
        self.Bind(wx.EVT_BUTTON, self.OnClickAnalysis, analysisButton)
        analysisButton.SetSize((200, 100))        
        analysisButton.SetToolTipString("Open the Graph Analysis interface.")
        # # #
        
        self.Bind(wx.EVT_MENU, self.OnAbout, menuAbout)
        self.Bind(wx.EVT_MENU, self.OnExit, menuExit)

        self.Show(True)

    def OnAbout(self,event):
        info = wx.AboutDialogInfo()
        info.Name = "GraphMaster 9000" 
        info.Version = "1.0"
        info.Copyright = "(C) 2014 Graph Enthusiasts Everywhere"
        info.Description = wordwrap("\nWith great power comes great responsability.", 350, wx.ClientDC(self))
        info.Developers = ["Francois Lebeau"]
        wx.AboutBox(info)
        
    def OnExit(self,event):
        self.Close(True)
        
    def OnClickEditor(self, event):
        G = nx.Graph()
        G.add_node('0')
        G.name = "New Graph"
        if self.path != None:
            try:
                src.writeGraph(G, self.path)
            except:
                pass
            editorWindow = EditorWindow(None, "GraphEditor 9000", G, self.path)
            editorWindow.Show(True)
        else:
            dlg = wx.MessageDialog(self, 'Please set a working directory first.',
                               'Error', wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        
    def OnClickAnalysis(self, event):
        if self.path != None:
            analysisWindow = AnalysisWindow(None, "GraphAnalysis 9000", self.path)
            analysisWindow.Show(True)
        else:
            dlg = wx.MessageDialog(self, 'Please set a working directory first.',
                               'Error',
                               wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
    def OnPath(self, event):
        dlg = wx.DirDialog(self, "Choose a directory:",
                          style=wx.DD_DEFAULT_STYLE)

        if dlg.ShowModal() == wx.ID_OK:
            self.path = dlg.GetPath()

        dlg.Destroy()  
        
class EditorWindow(wx.Frame):
    def __init__(self, parent, title, G, path):
        wx.Frame.__init__(self, parent, title=title, size=(378, 495))
        panel = wx.Panel(self)
        self.CreateStatusBar()
        self.path = path
        self.graphList = self.GetGraphList(self.path)
        self.graphTitleList = self.GetGraphTitleList(self.graphList)
        self.graph = G
        # # # Menu # # # 
        menuBar = wx.MenuBar() 
        
        filemenu= wx.Menu()        
        menuNew = filemenu.Append(wx.ID_CLEAR, "New Graph", "Create a new graph from scratch")        
        menuLoad = filemenu.Append(wx.ID_OPEN, "Load Graph", "Load an existing graph")
        filemenu.AppendSeparator()
        menuSave = filemenu.Append(wx.ID_SAVE, "&Save Graph\tCTRL+S", "Save current graph")

        menuPath = filemenu.Append(wx.ID_ANY, "Choose Path", "Choose working directory")
        filemenu.AppendSeparator()
        menuExit = filemenu.Append(wx.ID_EXIT,"E&xit","Close this window")
                
        menuBar.Append(filemenu,"&File")
        self.SetMenuBar(menuBar)
        # # #
        
        # # # Menu Events # # #
        self.Bind(wx.EVT_MENU, self.OnNew, menuNew)        
        self.Bind(wx.EVT_MENU, self.OnLoad, menuLoad)
        self.Bind(wx.EVT_MENU, self.OnSave, menuSave)
        self.Bind(wx.EVT_MENU, self.OnPath, menuPath)
        self.Bind(wx.EVT_MENU, self.OnExit, menuExit)
        # # #
        
        # # # Labels and Buttons # # #        
        self.graphTitleText = wx.StaticText(panel, wx.ID_ANY, "Graph title : " + G.name, (30, 30))
        titleButton = wx.Button(panel, wx.ID_ANY, "Modify", (255, 25))
        wx.StaticBox(panel, -1, "Current graph", size = (350, 400), pos = (5, 5))
           
        self.nodeText = wx.StaticText(panel, wx.ID_ANY, "Number of nodes : " + str(nx.number_of_nodes(G)), (30, 60)) 
        nodeButton = wx.Button(panel, wx.ID_ANY, "Add/remove nodes", (220, 55))
        self.edgeText = wx.StaticText(panel, wx.ID_ANY, "Number of edges : " + str(nx.number_of_edges(G)), (30, 90))

        self.cycleText = wx.StaticText(panel, wx.ID_ANY, "Number of cycles in basis : " + str(len(nx.cycle_basis(G))), (30, 120))
        cycleButton = wx.Button(panel, wx.ID_ANY, "Details", (255, 115))
        wx.StaticText(panel, wx.ID_ANY, "Degree distribution : ", (30, 150)) 
        degreeButton = wx.Button(panel, wx.ID_ANY, "View", (255, 145)) 
        
        self.diameterText = wx.StaticText(panel, wx.ID_ANY, "Diameter : " + str(nx.diameter(G)), (30, 180))         
        
        self.radiusText = wx.StaticText(panel, wx.ID_ANY, "Radius : " + str(nx.radius(G)), (30, 230)) 
        
        self.densityText = wx.StaticText(panel, wx.ID_ANY, "Density : " + str(nx.density(G)), (30, 260))
    
        self.copText = wx.StaticText(panel, wx.ID_ANY, "Cop number : " + self.GetCopNumber(G), (30, 290))
        
        drawButton = wx.Button(panel, wx.ID_ANY, "Draw Graph", (125, 325))
        saveButton = wx.Button(panel, wx.ID_ANY, "Save Graph", (125, 365))
        # # #
        
        # # # Button Events # # #   
        self.Bind(wx.EVT_BUTTON, self.OnModifyTitle, titleButton)        
        self.Bind(wx.EVT_BUTTON, self.OnModifyNodes, nodeButton) 
        self.Bind(wx.EVT_BUTTON, self.OnViewCycle, cycleButton)       
        self.Bind(wx.EVT_BUTTON, self.OnViewDegree, degreeButton)
        self.Bind(wx.EVT_BUTTON, self.OnDraw, drawButton)
        self.Bind(wx.EVT_BUTTON, self.OnSave, saveButton)

    def OnNew(self, event):
        G = nx.Graph()
        G.add_node('0')
        G.name = "New Graph"
        try:
            src.writeGraph(G, self.path)
        except Exception:
            pass
        editorWindow = EditorWindow(None, "GraphEditor 9000", G, self.path)
        editorWindow.Show(True)
    def OnLoad(self, event):
        try:
            dlg = wx.SingleChoiceDialog(
                self, 'Choose a graph to load from the following list :', 'Load Graph',
                self.graphTitleList, 
                wx.CHOICEDLG_STYLE
                )
    
            if dlg.ShowModal() == wx.ID_OK:
                editorWindow = EditorWindow(None, "GraphEditor 9000", src.loadGraph(dlg.GetStringSelection(), self.path), self.path)
                editorWindow.Show(True)
                self.Close(True)
        except IOError:
            dlg = wx.MessageDialog(self, 'No such file exist.',
                               'Error',
                               wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
        dlg.Destroy()
    def OnSave(self, event):
        try:
            src.writeGraph(self.graph, self.path)
            dlg = wx.MessageDialog(self, self.graph.name + " has been successfully saved.",
                               'Success',
                               wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        except Exception:
            dlg = wx.MessageDialog(self, self.graph.name + " : a graph with the same name already exists.",
                               'Error',
                               wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            
    def OnPath(self, event):
        dlg = wx.DirDialog(self, "Choose a directory:",
                          style=wx.DD_DEFAULT_STYLE)

        if dlg.ShowModal() == wx.ID_OK:
            self.path = dlg.GetPath()

        dlg.Destroy()        
    def OnExit(self,event):
        self.Close(True)
    
    def OnModifyTitle(self,event):
        dlg = wx.TextEntryDialog(
                self, 'Enter a new graph title :',
                'Modify graph title')

        dlg.SetValue(self.graph.name)

        if dlg.ShowModal() == wx.ID_OK:
            self.graph.name = dlg.GetValue()
            self.graphTitleText.SetLabel("Graph title : " + self.graph.name)

        dlg.Destroy()
    def OnModifyNodes(self,event):
        
        nodeFrame = NodeFrame(self, "Modify nodes", self.graph)
        nodeFrame.Show(True)
    
    def OnViewCycle(self, event):
        dlg = wx.MessageDialog(self, "Cycle basis : " + str(nx.cycle_basis(self.graph)) + "\nMinimal cycle length : " + str(src.getGirth(self.graph)) + "\nMaximal cycle length : " + str(src.getMaxCycleLength(self.graph)),
                               'Degree distribution',
                               wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()
    def OnViewDegree(self, event):
        dlg = wx.MessageDialog(self, "Degree distribution : " + str(nx.degree(self.graph)) + "\nMinimal degree : " + str(min(nx.degree(self.graph).values())) + "\nMaximal degree : " + str(max(nx.degree(self.graph).values())),
                               'Degree distribution',
                               wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()    
    def OnDraw(self, event):        
        self.drawGraph()
    def GetCopNumber(self, G):
        return str(src.readGraph(self.path + "\\" + G.name + ".txt")["copwin"])
    def GetGraphList(self, path):
        graphList = os.listdir(self.path)
        for graph in graphList:
            if not graph.endswith(".txt"):
                graphList.remove(graph)
        return graphList        
    def GetGraphTitleList(self, graphList):
        graphTitleList = []
        for graph in graphList:
            graphTitleList.append(src.readGraph(self.path + '\\' + graph)["title"])
        return graphTitleList
    def drawGraph(self):
        nx.draw(self.graph)
        plt.show()
    
class AnalysisWindow(wx.Frame):
    def __init__(self, parent, title, path):
        wx.Frame.__init__(self, parent, title=title, size=(1000,800))
        panel = wx.Panel(self)
        self.CreateStatusBar()
        self.path = path        
        self.graphList = self.GetGraphList(self.path)
        self.graphListCopy = copy.copy(self.graphList)
        self.graphTitleList = self.GetGraphTitleList(self.graphList)
        relationList = ["<", "<=", "==", ">=", ">", "!="]
        menuBar = wx.MenuBar() 
        
        filemenu= wx.Menu()     
        menuPath = filemenu.Append(wx.ID_ANY, "Choose Path", "Choose working directory")
        filemenu.AppendSeparator()
        menuExit = filemenu.Append(wx.ID_EXIT,"E&xit","Close this window")
        menuBar.Append(filemenu,"&File")
        self.SetMenuBar(menuBar)
        self.Bind(wx.EVT_MENU, self.OnPath, menuPath)
        self.Bind(wx.EVT_MENU, self.OnExit, menuExit)
        
        wx.StaticBox(panel, -1, "Choose your search criterias", size = (700, 700), pos = (5, 5))
        self.listBox = wx.ListBox(panel, 60, (740, 25), (200, 600), self.graphTitleList, wx.LB_SINGLE)

        filterButton = wx.Button(panel, wx.ID_ANY, "Filter", (795, 640))  
        openButton = wx.Button(panel, wx.ID_ANY, "Open in Graph Editor", (773, 675))
        
        #Nodes
        self.nodesCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 40), (40, 20), wx.NO_BORDER)
        self.nodesCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 40), (40, -1), relationList, wx.CB_DROPDOWN)
        self.nodesSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 40))
        wx.StaticText(panel, wx.ID_ANY, "nodes.", (280, 45))                 
        
        #Diameter
        self.diameterCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 80), (40, 20), wx.NO_BORDER)
        self.diameterCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 80), (40, -1), relationList, wx.CB_DROPDOWN)
        self.diameterSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 80))
        wx.StaticText(panel, wx.ID_ANY, "diameter.", (280, 85))     
        
        #Radius
        self.radiusCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 120), (40, 20), wx.NO_BORDER)
        self.radiusCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 120), (40, -1), relationList, wx.CB_DROPDOWN)
        self.radiusSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 120))
        wx.StaticText(panel, wx.ID_ANY, "radius.", (280, 125))
        
        #Cycles
        self.cycleCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 160), (40, 20), wx.NO_BORDER)
        self.cycleCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 160), (40, -1), relationList, wx.CB_DROPDOWN)
        self.cycleSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 160))
        wx.StaticText(panel, wx.ID_ANY, "cycles.", (280, 165))
        
        #Minimal cycle length
        self.mincyclelengthCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 200), (40, 20), wx.NO_BORDER)
        self.mincyclelengthCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 200), (40, -1), relationList, wx.CB_DROPDOWN)
        self.mincyclelengthSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 200))
        wx.StaticText(panel, wx.ID_ANY, "minimal cycle length.", (280, 205))
        
        #Maximal cycle length
        self.maxcyclelengthCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 240), (40, 20), wx.NO_BORDER)
        self.maxcyclelengthCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 240), (40, -1), relationList, wx.CB_DROPDOWN)
        self.maxcyclelengthSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 240))
        wx.StaticText(panel, wx.ID_ANY, "maximal cycle length.", (280, 245))
        
        #Density
        self.densityCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 280), (40, 20), wx.NO_BORDER)
        self.densityCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 280), (40, -1), relationList, wx.CB_DROPDOWN)
        self.densitySpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 280))
        wx.StaticText(panel, wx.ID_ANY, "density (rounded at the first decimal and multiplied by 10).", (280, 285))
        
        #minDegree
        self.mindegreeCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 320), (40, 20), wx.NO_BORDER)
        self.mindegreeCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 320), (40, -1), relationList, wx.CB_DROPDOWN)
        self.mindegreeSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 320))
        wx.StaticText(panel, wx.ID_ANY, "for minimal degree.", (280, 325))
        
        #maxDegree
        self.maxdegreeCheck = wx.CheckBox(panel, wx.ID_ANY, "Has", (45, 360), (40, 20), wx.NO_BORDER)
        self.maxdegreeCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 360), (40, -1), relationList, wx.CB_DROPDOWN)
        self.maxdegreeSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 360))
        wx.StaticText(panel, wx.ID_ANY, "for maximal degree.", (280, 365))
        
        #Copwin
        self.copwinCheck = wx.CheckBox(panel, wx.ID_ANY, "Is", (45, 400), (40, 20), wx.NO_BORDER)
        self.copwinCombo = wx.ComboBox(panel, wx.ID_ANY, relationList[0], (90, 400), (40, -1), relationList, wx.CB_DROPDOWN)
        self.copwinSpin = wx.SpinCtrl(panel, wx.ID_ANY, "", (140, 400))
        self.copwinSpin.SetRange(1, 10)
        wx.StaticText(panel, wx.ID_ANY, "cop-win (use != for NOT cop-win).", (280, 405))
        
        # # # Button Events # # #
        self.Bind(wx.EVT_LISTBOX_DCLICK, self.EvtListBoxDClick, self.listBox)
        self.Bind(wx.EVT_BUTTON, self.OnFilter, filterButton)
        self.Bind(wx.EVT_BUTTON, self.OnLoad, openButton)   
        # # #
          
    def EvtListBoxDClick(self, event):
        editorWindow = EditorWindow(None, "GraphEditor 9000", src.loadGraph(self.listBox.GetStringSelection(), self.path), self.path)
        editorWindow.Show(True)
    def OnPath(self, event):
        dlg = wx.DirDialog(self, "Choose a directory:",
                          style=wx.DD_DEFAULT_STYLE)
        if dlg.ShowModal() == wx.ID_OK:
            self.path = dlg.GetPath()
            self.graphList = self.GetGraphList(self.path)
    def OnExit(self,event):
        self.Close(True)
    def OnFilter(self, event):
        filterDict = {}
        self.graphList = copy.copy(self.graphListCopy)
        self.graphTitleList = self.GetGraphTitleList(self.graphList)
        if self.nodesCheck.IsChecked():
            filterDict["nodes"] = (self.nodesCombo.GetValue(), self.nodesSpin.GetValue())
        if self.diameterCheck.IsChecked():
            filterDict["diameter"] = (self.diameterCombo.GetValue(), self.diameterSpin.GetValue())
        if self.radiusCheck.IsChecked():
            filterDict["radius"] = (self.radiusCombo.GetValue(), self.radiusSpin.GetValue())
        if self.cycleCheck.IsChecked():
            filterDict["cycles"] = (self.cycleCombo.GetValue(), self.cycleSpin.GetValue())
        if self.mincyclelengthCheck.IsChecked():
            filterDict["mincyclelength"] = (self.mincyclelengthCombo.GetValue(), self.mincyclelengthSpin.GetValue())
        if self.maxcyclelengthCheck.IsChecked():
            filterDict["maxcyclelength"] = (self.maxcyclelengthCombo.GetValue(), self.maxcyclelengthSpin.GetValue())
        if self.densityCheck.IsChecked():
            filterDict["density"] = (self.densityCombo.GetValue(), self.densitySpin.GetValue())
        if self.mindegreeCheck.IsChecked():
            filterDict["mindegree"] = (self.mindegreeCombo.GetValue(), self.mindegreeSpin.GetValue())
        if self.maxdegreeCheck.IsChecked():
            filterDict["maxdegree"] = (self.maxdegreeCombo.GetValue(), self.maxdegreeSpin.GetValue())
        if self.copwinCheck.IsChecked():            
            filterDict["copwin"] = (self.copwinCombo.GetValue(), self.copwinSpin.GetValue())
        
        self.graphList = src.graphFilter(self.graphList, filterDict, self.path)
        self.graphTitleList = self.GetGraphTitleList(self.graphList)
        self.listBox.Set(self.graphTitleList)
    def OnLoad(self, event):
        editorWindow = EditorWindow(None, "GraphEditor 9000", src.loadGraph(self.listBox.GetStringSelection(), self.path), self.path)
        editorWindow.Show(True)
    
    def GetGraphList(self, path):
        graphList = os.listdir(self.path)
        for graph in graphList:
            if not graph.endswith(".txt"):
                graphList.remove(graph)
        return graphList        
    def GetGraphTitleList(self, graphList):
        graphTitleList = []
        for graph in graphList:
            graphTitleList.append(src.readGraph(self.path + '\\' + graph)["title"])
        return graphTitleList
    
class NodeFrame(wx.MiniFrame):
    def __init__(self, parent, title, G):
        wx.Frame.__init__(self, parent, title=title, size=(425, 300))
        self.graph = G
        self.parent = parent
        panel = wx.Panel(self)
        self.nodeList = self.GetNodeList(self.graph)
        self.edgeList = self.GetEdgeList(self.graph)        
        # # #
        wx.StaticText(panel, -1, "Node list :", (10, 5))
        wx.StaticText(panel, -1, "Edge list :", (10, 130))
        # # #
        nodeButton = wx.Button(panel, -1 ,"Node list", (300,5))
        edgeButton = wx.Button(panel, -1 ,"Edge list", (300,35))
        okButton = wx.Button(panel, -1, "Ok", (215, 225) )
        cancelButton = wx.Button(panel, -1, "Cancel", (315, 225))
        removeNodeButton = wx.Button(panel, -1, "Remove Node", (100, 100))
        removeEdgeButton = wx.Button(panel, -1, "Remove Edge", (100, 225))
        addNodeButton = wx.Button(panel, -1, "Add Node", (100, 50))
        addEdgeButton = wx.Button(panel, -1, "Add Edge", (100, 175))
        self.listBoxNodes = wx.ListBox(panel, -1, (10, 25), (80, 100), self.nodeList, wx.LB_SINGLE)
        self.listBoxEdges = wx.ListBox(panel, -1, (10, 150), (80, 100), self.edgeList, wx.LB_SINGLE)
        # # #
        self.Bind(wx.EVT_BUTTON, self.OnNodeList, nodeButton)        
        self.Bind(wx.EVT_BUTTON, self.OnEdgeList, edgeButton)
        self.Bind(wx.EVT_BUTTON, self.OnOK, okButton)
        self.Bind(wx.EVT_BUTTON, self.OnCancel, cancelButton)
        self.Bind(wx.EVT_BUTTON, self.OnRemoveNode, removeNodeButton)
        self.Bind(wx.EVT_BUTTON, self.OnRemoveEdge, removeEdgeButton)
        self.Bind(wx.EVT_BUTTON, self.OnAddNode, addNodeButton)
        self.Bind(wx.EVT_BUTTON, self.OnAddEdge, addEdgeButton)
    def GetNodeList(self, G):
        nodeList = nx.nodes(G)
        try:
            for node in nodeList :
                nodeList[node] = str(node)
        except TypeError:
            return nodeList
        return nodeList
    def GetEdgeList(self, G):
        edgeList = nx.edges(G)
        i = 0
        for edge in edgeList:
            edgeList[i] = str(edge)
            i += 1
        return edgeList
    def OnAddNode(self, event):
        dlg = wx.Dialog(self, -1, "Add a node", (350, 200),
                         style=wx.DEFAULT_DIALOG_STYLE,
                         )
        
        wx.StaticText(dlg, -1, "Add node :", (95,30))
        nodeAdded = wx.TextCtrl(dlg, -1, "", (190, 28))
        
        wx.StaticText(dlg, -1, "Connected to :", (95, 100))
        nodeConnected = wx.TextCtrl(dlg, -1, "", (190, 98))
        
        wx.Button(dlg,  wx.ID_OK, "Ok", (95, 155) )
        wx.Button(dlg, wx.ID_CANCEL, "Cancel", (215, 155))
        
        val = dlg.ShowModal()
        if val == wx.ID_OK:
            nodeAddedEntered = str(nodeAdded.GetValue())
            nodeConnectedEntered = str(nodeConnected.GetValue())
            if nodeAddedEntered == nodeConnectedEntered:
                dlg1 = wx.MessageDialog(self, "Connect to a node that is already in the graph.",
                               'Error',
                               wx.OK
                               )
                dlg1.ShowModal()
                dlg1.Destroy()
            if nodeAddedEntered in nx.nodes(self.graph):
                dlg2 = wx.MessageDialog(self, "The node you entered is already in the graph.",
                               'Error',
                               wx.OK
                               )
                dlg2.ShowModal()
                dlg2.Destroy()
             
            if nodeConnectedEntered not in nx.nodes(self.graph):
                dlg3 = wx.MessageDialog(self, "You cannot connect to a node that isn't in the graph.",
                               'Error',
                               wx.OK
                               )
                dlg3.ShowModal()
                dlg3.Destroy()
            
            if nodeAddedEntered not in nx.nodes(self.graph) and nodeConnectedEntered in nx.nodes(self.graph) :   
                self.graph.add_node(nodeAddedEntered)
                self.graph.add_edges_from([(nodeAddedEntered, nodeConnectedEntered)])
                self.nodeList = self.GetNodeList(self.graph)
                self.listBoxNodes.Set(self.nodeList)   
                self.edgeList = self.GetEdgeList(self.graph)
                self.listBoxEdges.Set(self.edgeList)
        else:
            pass
        dlg.Destroy()
    def OnAddEdge(self, event):
        dlg = wx.Dialog(self, -1, "Add an edge", (350, 200),
                         style=wx.DEFAULT_DIALOG_STYLE,
                         )
        
        wx.StaticText(dlg, -1, "Add edge connecting :", (65,30))
        firstNode = wx.TextCtrl(dlg, -1, "", (200, 28))
        
        wx.StaticText(dlg, -1, "And :", (65, 100))
        secondNode = wx.TextCtrl(dlg, -1, "", (200, 98))
        
        wx.Button(dlg,  wx.ID_OK, "Ok", (95, 155) )
        wx.Button(dlg, wx.ID_CANCEL, "Cancel", (215, 155))
        
        val = dlg.ShowModal()
        if val == wx.ID_OK:
            firstNodeEntered = str(firstNode.GetValue())
            secondNodeEntered = str(secondNode.GetValue())
            edge = (firstNodeEntered, secondNodeEntered)
            reverseEdge = (secondNodeEntered, firstNodeEntered)
            if edge in nx.edges(self.graph) or reverseEdge in nx.edges(self.graph):
                dlg2 = wx.MessageDialog(self, "The edge you entered is already in the graph.",
                               'Error',
                               wx.OK
                               )
                dlg2.ShowModal()
                dlg2.Destroy()
            elif firstNodeEntered not in nx.nodes(self.graph) or secondNodeEntered not in nx.nodes(self.graph):
                dlg2 = wx.MessageDialog(self, "The node you entered is not in the graph.",
                               'Error',
                               wx.OK
                               )
                dlg2.ShowModal()
                dlg2.Destroy()            
            else:  
                self.graph.add_edges_from([(firstNodeEntered, secondNodeEntered)])
            
                self.nodeList = self.GetNodeList(self.graph)
                self.listBoxNodes.Set(self.nodeList)   
                self.edgeList = self.GetEdgeList(self.graph)
                self.listBoxEdges.Set(self.edgeList)
        else:
            pass
        dlg.Destroy()
    def OnRemoveNode(self, event):
        if len(self.nodeList) == 1:
            dlg = wx.MessageDialog(self, "You cannot remove the last node from this graph.",
                               'Error',
                               wx.OK
                               )
            dlg.ShowModal()
            dlg.Destroy()   
        else:
            self.graph.remove_node(self.listBoxNodes.GetStringSelection())  
            
            self.nodeList = self.GetNodeList(self.graph)
            self.listBoxNodes.Set(self.nodeList)   
            self.edgeList = self.GetEdgeList(self.graph)
            self.listBoxEdges.Set(self.edgeList)
    def OnRemoveEdge(self, event):
        if len(self.edgeList) == 0:
            dlg = wx.MessageDialog(self, "No edge is selected.",
                               'Error',
                               wx.OK
                               )
            dlg.ShowModal()
            dlg.Destroy() 
        else:
            selection = eval(self.listBoxEdges.GetStringSelection())
            graphCopy = self.graph.copy()
            graphCopy.remove_edge(selection[0], selection[1])
            if nx.is_connected(graphCopy):
                self.graph.remove_edge(selection[0], selection[1])
                self.nodeList = self.GetNodeList(self.graph)
                self.listBoxNodes.Set(self.nodeList) 
                self.edgeList = self.GetEdgeList(self.graph)
                self.listBoxEdges.Set(self.edgeList)
            else:
                dlg = wx.MessageDialog(self, "Removing this edge would disconnect the graph.",
                                   'Error',
                                   wx.OK
                                   )
                dlg.ShowModal()
                dlg.Destroy()   
    def OnCancel(self,event):
        self.Close()    
    def OnOK(self, event):
        self.parent.graph = self.graph
        self.Close()
        self.parent.nodeText.SetLabel("Number of nodes : " + str(nx.number_of_nodes(self.graph)))
        self.parent.edgeText.SetLabel("Number of edges : " + str(nx.number_of_edges(self.graph)))
        self.parent.cycleText.SetLabel("Number of cycles in basis : " + str(len(nx.cycle_basis(self.graph))))
        self.parent.diameterText.SetLabel("Diameter : " + str(nx.diameter(self.graph)))
        self.parent.radiusText.SetLabel("Radius : " + str(nx.radius(self.graph))) 
        self.parent.densityText.SetLabel("Density : " + str(nx.density(self.graph)))
        print "Calculating cop number..."
        self.parent.copText.SetLabel("Cop number : " + self.GetCopNumber(self.graph))
        print "All done !"
    def OnNodeList(self, event):
            dlg = wx.MessageDialog(self, "Node list : " + str(nx.nodes(self.graph)),
                               'List of nodes',
                               wx.OK
                               )
            dlg.ShowModal()
            dlg.Destroy()   
    def OnEdgeList(self, event):
            dlg = wx.MessageDialog(self, "Edge list : " + str(nx.edges(self.graph)),
                               'List of edges',
                               wx.OK
                               )
            dlg.ShowModal()
            dlg.Destroy()
    def GetCopNumber(self, G): 
        return src.getCopNumber(G)
        
        
                
        